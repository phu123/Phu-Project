const express = require("express");
const fetch = require("node-fetch");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//
const filters = {
  productFilter: [
    "Mua nhiều giảm giá", // 0
    "Khuyến mãi", // 1
    "Chuyển phát Hỏa tốc", //2
    "...", //3,
  ],
};

app.get("/", (req, res) => {
  res.end("Hello world!");
});

app.get("/products", (req, res) => {
  console.log("get products page", req.query.p);
  console.log("get products query", req.query.q);
  console.log(req.body);
  const page = req.query.p || 1;
  fetch(`https://mapi.sendo.vn/mob/product/search?p=${page}&q=${req.query.q}`)
    .then((resData) => resData.json())
    .then((json) => res.json(json))
    .catch((err) => res.json([]));
});

app.get("/product/:id", (req, res) => {
  const { id } = req.params;
  console.log("get detail of ", id);
  fetch(`https://mapi.sendo.vn/mob/product/${id}/detail`)
    .then((resData) => resData.json())
    .then((json) => res.json(json))
    .catch((err) => res.json({}));
});

app.listen(4000, () => {
  console.log("Server is listening on port 4000");
});

const users = [
  { name: "Sĩ Phú", username: "siphu", password: "phu12345", age : 24 },
  { name: "Trung Hiếu", username: "trunghieu", password: "hieu12345", age : 24 },
  { name: "Đình Duy", username: "dinhduy", password: "duy12345", age : 22 },
  { name: "Đình Dũng", username: "dinhdung", password: "dung12345", age : 20 },
  { name: "Quang Vinh", username: "quangvinh", password: "vinh12345", age : 21 },
];

const sessions = [];

app.put("/login", (req, res) => {
  const { username, password } = req.body;
  console.log(req.body);

  const user = users.findIndex(
    (item) => item.username === username && item.password === password
  );
  console.log(user);

  if (user > -1) {
    // const token = loggedUser.username + "_" + Date.now();
    const token = Date.now();

    const userData = users[user];
    const session = { token, user: userData };
    sessions.push(session);
    return res.json(userData);
  } else res.json({ message: "Username or Password is incorrect" });
});
