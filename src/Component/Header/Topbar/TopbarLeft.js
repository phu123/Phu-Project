import React, {Component} from "react"
import "./TopbarLeft.css"

class TopbarLeft extends Component{
    render(){
        return(
            <div className="TopbarLeft">
                <ul className="TopbarLeft__list-main">
                    <li><i className="fas fa-headphones" /> +060 (800) 801-582</li>
                    <li><i className="far fa-envelope" /> support@phushop.com</li>
                </ul>
            </div>
        )
    }
}

export default TopbarLeft