import React, {Component} from "react"
import "./MainTopbar.css"
import TopbarLeft from "./TopbarLeft"
import TopbarRight from "./TopbarRight"

class Topbar extends Component{
    render(){
        return(
            <div className="Topbar">
                <div className="Topbar__container">
                    <div className="row">
                        <div className="col-lg-5 col-md-12 col-12">
                        {/* Top Left */}
                            <TopbarLeft />
                        {/*/ End Top Left */}
                        </div>
                        <div className="col-lg-7 col-md-12 col-12">
                        {/* Top Right */}
                            <TopbarRight />
                        {/* End Top Right */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Topbar