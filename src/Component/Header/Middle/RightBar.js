import React, {Component} from "react"
import "./RightBar.css"
import {connect} from "react-redux"
import {Link} from "react-router-dom"

class RightBar extends Component{
    render(){
        return(
          <div className="right-bar-div col-lg-2 col-md-3 col-12">
              <div className="right-bar">
                <div className="single-bar" style={{cursor: "pointer"}}>
                  <a className="single-icon"><i className="fa fa-heart-o" aria-hidden="true" /></a>
                </div>
                <div className="single-bar" style={{cursor: "pointer"}}>
                  <a className="single-icon"><i className="fa fa-user-circle-o" aria-hidden="true" /></a>
                </div>
                <div className="single-bar shopping">
                  <Link to="/cart">
                    <i className="fas fa-shopping-cart">
                      <span className="total-count">{this.props.quantityCart}</span>
                    </i>
                  </Link>
                </div>
              </div>
          </div>
        )
    }
}

const mapStateToProps = state =>({
  quantityCart: state.quantityCart
})

export default connect(mapStateToProps, null)(RightBar)