import React, {Component} from "react"
import "./Logo.css"

class Logo extends Component{
    render(){
        return(
            <div className="logoShop-div col-lg-2 col-md-2 col-12">
                <div className="logoShop">
                    <a href="index.html">Pshop<span>.</span></a>
                </div>
            </div>
        )
    }
}

export default Logo