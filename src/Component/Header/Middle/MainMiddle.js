import React, {Component} from "react"
import Logo from "./Logo"
import Category from "./Category"
import RightBar from "./RightBar"
import "./MainMiddle.css"

class MainMiddle extends Component{
    render(){
        return(
            <div className="MiddleInner">
                <div className="container">
                    <div className="MiddleInner-flex">                       
                        <Logo />                  
                        <Category /> 
                        <RightBar />                       
                    </div>
                </div>
            </div>
        )
    }
}

export default MainMiddle