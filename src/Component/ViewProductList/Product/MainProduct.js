import React,{Component} from "react"
import ShopTop from "./ShopTop"
import SingleProduct from "./SingleProduct"
import "./MainProduct.css"

class MainProduct extends Component{
    render() {
        return (
            <div className="main-product col-lg-9 col-md-8 col-12">
                <ShopTop />
                <SingleProduct />
            </div>
        );
    }
}

export default MainProduct