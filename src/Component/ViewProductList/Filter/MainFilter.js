import React,{Component} from "react"
import FilterPrice from "./FilterPrice"
import FilterProduct from "./FilterProduct"
import LocationShop from "./LocationShop"
import "./MainFilter.css"

class MainFilter extends Component{
    render(){
        return(
            <div className="main-filter col-lg-3 col-md-4 col-12">
                <FilterPrice  />
                <FilterProduct />
                <LocationShop />
            </div>
        )
    }
}

export default MainFilter