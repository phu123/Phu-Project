import React, {Component} from "react"
import {connect} from "react-redux"
import {getData, filterCheckbox} from "./../../../redux/actions"
import "./FilterProduct.css"

class FilterProduct extends Component{
    render() {
        return (
            <div className ="filterProduct">
                <h3 className="title">BỘ LỌC SẢN PHẨM</h3>
                <div>            
                    <input type="checkbox" id="discount" onClick={()=>this.props.filterCheckbox("is_promotion",true)}/>
                    <label htmlFor="discount">Khuyến mãi</label>
                </div>  
                <div>
                    <input type="checkbox" id="event" onClick={()=>this.props.filterCheckbox("is_event",true)}/>
                    <label htmlFor="event">Sự kiện</label>
                </div> 
                <div>
                    <input type="checkbox" id="video" onClick={()=>this.props.filterCheckbox("has_video",0)}/>
                    <label htmlFor="video">Không quảng cáo</label>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    productList : state.productList,
    obj : state.obj,
    uncheck : state.uncheck
})

const mapDispatchToProps = dispatch =>({
    getData: ()=> dispatch(getData()),
    filterCheckbox:(key, value)=> dispatch(filterCheckbox(key, value))
})

export default connect(mapStateToProps, mapDispatchToProps)(FilterProduct)