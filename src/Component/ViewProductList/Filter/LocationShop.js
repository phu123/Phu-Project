import React,{Component} from "react"
import "./LocationShop.css"
import {connect} from "react-redux"
import {filterLocation} from "./../../../redux/actions"

class LocationShop extends Component{
    render() {
        return (
            <div className ="location-shop">
                <h3 className="title">VỊ TRÍ SHOP</h3>
                <div>            
                    <input type="checkbox" id="discount" name="location" onClick={()=>this.props.filterLocation("Hồ Chí Minh")} />
                    <label htmlFor="discount">Hồ Chí Minh</label>
                </div>  
                <div>
                    <input type="checkbox" id="review" name="location" onClick={()=>this.props.filterLocation("Hà Nội")} />
                    <label htmlFor="review">Hà Nội</label>
                </div> 
                <div>
                    <input type="checkbox" id="video" name="location" onClick={()=>this.props.filterLocation("Đà Nẵng")} />
                    <label htmlFor="video">Đà Nẵng</label>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch =>({
    filterLocation: (local)=>dispatch(filterLocation(local))
})

export default connect(null, mapDispatchToProps)(LocationShop)