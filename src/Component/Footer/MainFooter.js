import React, {Component} from "react"
import InformationContact from "./InformationContact"
import SingleWidget from "./SingleWidget"
import MainContact from "./Contact/MainContact"
import MainCopyright from "./Copyright/MainCopyright"
import "./MainFooter.css"
import NewsLetter from "./NewsLetter"

class MainFooter extends Component{
    render(){
        return(
            <div>
                <div>
                    <NewsLetter />
                </div>
                <div className="row main-footer">
                    <InformationContact />
                    <SingleWidget />
                    <MainContact />
                    <MainCopyright />
                </div>
            </div>
        )
    }
}

export default MainFooter