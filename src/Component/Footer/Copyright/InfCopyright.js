import React,{Component} from "react"
import "./InfCopyright.css"

class InfCopyright extends Component{
    render() {
        return (
            <div className="col-lg-6 col-12 InfCopyright">
                <div className="InfCopyright">
                    <p>Bản Quyền © 2020 <span>PSHOP</span> -  Đã Đăng Ký Bản Quyền.</p>
                </div>
            </div>
        )
    }
}

export default InfCopyright