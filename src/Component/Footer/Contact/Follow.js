import React,{Component} from "react"
import "./Follow.css"

class Follow extends Component{
    render() {
        return (
            <ul className="follow">
                <li><a href="#"><i className="fab fa-facebook-f" /></a></li>
                <li><a href="#"><i className="fab fa-twitter" /></a></li>
                <li><a href="#"><i className="fab fa-flickr" /></a></li>
                <li><a href="#"><i className="fab fa-instagram" /></a></li>
            </ul>
        );
    }
}

export default Follow