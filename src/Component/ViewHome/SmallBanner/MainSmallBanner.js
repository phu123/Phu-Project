import React, {Component} from "react"
import "./MainSmallBanner.css"
import {connect} from "react-redux"
import {Link} from "react-router-dom"
import {search} from "./../../../redux/actions"

class MainSmallBanner extends Component{
    render(){
        return(
            <div className="main-small-banner section">
                <div className="container-fluid">
                    <div className="row">
                        {/* Single Banner  */}
                        <div className="col-lg-4 col-md-6 col-12">
                            <div className="single-banner">
                                <img src={require('./ImageSmallBanner/mini-banner1.jpg')} alt="#" />
                                <div className="content">
                                    <p>Đồ Nam</p>
                                    <h3>Quần Áo <br /> Du Lịch Hè</h3>
                                    <Link onClick={()=> this.props.search("quan ao du lich mua he")} to="/list">Khám Phá Ngay</Link>
                                </div>
                            </div>
                        </div>
                        {/* /End Single Banner  */}
                        {/* Single Banner  */}
                        <div className="col-lg-4 col-md-6 col-12">
                            <div className="single-banner">
                                <img src={require('./ImageSmallBanner/mini-banner2.jpg')} alt="#" />
                                <div className="content">
                                    <p>Túi Xách</p>
                                    <h3>Mẫu Mới <br /> 2020</h3>
                                    <Link onClick={()=> this.props.search("tui xach")} to="/list">Mua Ngay</Link>
                                </div>
                            </div>
                        </div>
                        {/* /End Single Banner  */}
                        {/* Single Banner  */}
                        <div className="col-lg-4 col-12">
                            <div className="single-banner tab-height">
                                <img src={require('./ImageSmallBanner/mini-banner3.jpg')} alt="#" />
                                <div className="content">
                                    <p>Giảm Giá</p>
                                    <h3>Đón Hè <br /> Giảm Đến <span>40%</span></h3>
                                    <Link onClick={()=> this.props.search("quan ao giam gia")} to="/list">Khám Phá Ngay</Link>
                                </div>
                            </div>
                        </div>
                        {/* /End Single Banner  */}
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch=>({
    search: (query)=>dispatch(search(query))
})

export default connect(null, mapDispatchToProps)(MainSmallBanner)