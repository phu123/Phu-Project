import React,{Component} from "react"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Slider from "react-slick"
import {connect} from "react-redux"
import {dataPreview} from "./../../../redux/actions"
import "./MainDiscountProduct.css"
import truncate from 'lodash/truncate';
import sanitizeHtml from 'sanitize-html';
import {Link} from "react-router-dom"

class MainDiscountProduct extends Component{
    componentDidMount=()=>{
        this.props.dataPreview(1,"ao nam")
    }
    render() {
        const settings = {
            className: "center",
            centerMode: true,
            infinite: true,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            centerPadding: "60px",
        };
        const {productPreview} = this.props
        return (
            <div className="main-discount-product">
                <h5>Đề cử cho bạn</h5>
                <Slider {...settings}>
                    {productPreview.map((products, index)=>
                        <div className="main-discount-product__single" key={index}>
                            <Link to={`/detail?id=${products.product_id}`} style={{textDecoration: "none"}}><div className="product-preview">
                                <img src={products.img_url}/>
                                <div className="product-preview__name">{truncate(sanitizeHtml(products?.name),{length: 20, separator: /,? +/, })}</div>
                                <div className="product-preview__price">{products.original_price}</div>
                            </div></Link>
                        </div>
                    )}
                </Slider>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    productPreview : state.productPreview
})

const mapDispatchToProps = dispatch =>({
    dataPreview:(page, query)=>dispatch(dataPreview(page, query))
})


export default connect(mapStateToProps, mapDispatchToProps)(MainDiscountProduct)