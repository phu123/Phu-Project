import React,{Component} from "react"
import SectionTitle from "./SectionTitle"
import NavMain from "./NavMain"
import "./MainProductInfo.css"
import SingleProductInfo from "./SingleProductInfo"

class MainProductInfo extends Component{
    render(){
        return(
            <div className="main-product-info"> 
               <SectionTitle />
               <NavMain />
               <SingleProductInfo />
            </div>
        )
    }
}

export default MainProductInfo