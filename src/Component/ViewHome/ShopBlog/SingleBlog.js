import React, {Component} from "react"
import "./SingleBlog.css"
import {Link} from "react-router-dom"
import {connect} from "react-redux"
import {search} from "./../../../redux/actions"

const singleBlog =[
    {
        imgsrc : require('./imageShopBlog/blog1.jpg'),
        date : "18 Tháng 5, 2020. Thứ Hai",
        title : "Phụ Kiện Thời Trang",
        more : "Tiếp Tục Mua",
        search : "phu kien thoi trang"
    },
    {
        imgsrc : require('./imageShopBlog/blog2.jpg'),
        date : "18 Tháng 5, 2020. Thứ Hai",
        title : "Ngày Hội Mua Sắm",
        more : "Tiếp Tục Mua",
        search : "quan ao"
    },
    {
        imgsrc : require('./imageShopBlog/blog3.jpg'),
        date : "18 Tháng 5, 2020. Thứ Hai",
        title : "Lễ Hội Thời Trang Nữ",
        more : "Tiếp Tục Mua",
        search : "thoi trang nu"
    },
]
class SingleBlog extends Component{
    render(){
        return(
            <div className="row container__div">
                {singleBlog.map((item, index)=>(
                    <div className="col-lg-4 col-md-6 col-12" key={index}>
                    {/* Start Single Blog  */}
                        <div className="shop-single-blog">
                            <img src={item.imgsrc} alt="#" />
                            <div className="content">
                            <p className="date">{item.date}</p>
                            <a className="title">{item.title}</a>
                            <Link to="/list" onClick={()=>this.props.search(item.search)} className="more-btn">{item.more}</Link>
                        </div>
                    </div>
                    {/* End Single Blog  */}
                    </div>
                ))}                
            </div>
        )
    }
}

const mapDispatchToProps = dispatch =>({
    search: (query )=>dispatch(search(query))
})

export default connect(null, mapDispatchToProps)(SingleBlog)