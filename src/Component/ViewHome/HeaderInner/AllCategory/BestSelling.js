import React, {Component} from "react"
import "./BestSelling.css"
import {Link} from "react-router-dom"
import {connect} from "react-redux"
import { search} from "./../../../../redux/actions"

class BestSelling extends Component{
    render() {
        return (
            <li className="best-selling option"><a>giá tốt <i className="fa fa-angle-right" aria-hidden="true" /></a>
                <ul className="mega-menu">
                    <li className="single-menu">
                        <p className="title-link">Shop Trẻ Em</p>
                        <div className="image">
                            <img src={require('./imagesmega/mega-menu1.jpg')} alt="#" />
                        </div>
                        <div className="inner-link">
                            <Link to="/list" onClick={()=>this.props.search("ao tre em")}>Áo Trẻ Em</Link>
                            <Link to="/list" onClick={()=>this.props.search("quan tre em")}>Quần Trẻ Em</Link>
                            <Link to="/list" onClick={()=>this.props.search("mu tre em")}>Mũ Trẻ Em</Link>
                            <Link to="/list" onClick={()=>this.props.search("giay tre em")}>Giày Trẻ Em</Link>
                        </div>
                        </li>
                        <li className="single-menu">
                        <p className="title-link">Shop Nam</p>
                        <div className="image">
                            <img src={require("./imagesmega/mega-menu2.jpg")} alt="#" />
                        </div>
                        <div className="inner-link">
                            <Link to="/list" onClick={()=>this.props.search("ao nam")}>Áo Nam</Link>
                            <Link to="/list" onClick={()=>this.props.search("quan nam")}>Quần Nam</Link>
                            <Link to="/list" onClick={()=>this.props.search("mu nam")}>Mũ Nam</Link>
                            <Link to="/list" onClick={()=>this.props.search("giay nam")}>Giày Nam</Link>
                        </div>
                        </li>
                        <li className="single-menu">
                        <p className="title-link">Shop Nữ</p>
                        <div className="image">
                            <img src={require("./imagesmega/mega-menu3.jpg")} alt="#" />
                        </div>
                        <div className="inner-link">
                            <Link to="/list" onClick={()=>this.props.search("ao nu")}>Áo Nữ</Link>
                            <Link to="/list" onClick={()=>this.props.search("quan nu")}>Quần Nữ</Link>
                            <Link to="/list" onClick={()=>this.props.search("mu nu")}>Mũ Nữ</Link>
                            <Link to="/list" onClick={()=>this.props.search("giay nu")}>Giày Nữ</Link>
                        </div>
                    </li>
                </ul>
            </li>
        );
    }
}

const mapDispatchToProps = dispatch =>({
    search: (query)=> dispatch(search(query))
})

export default connect(null, mapDispatchToProps)(BestSelling)