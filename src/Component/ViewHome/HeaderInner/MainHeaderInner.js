import React, {Component} from "react"
import MainAllCategory from "./AllCategory/MainAllCategory"
import MenuArea from "./MenuArea"
import "./MainHeaderInner.css"

class MainHeaderInner extends Component{
    render(){
        return(
            <div className="main-header-inner">
                <div className="container">
                    <div className="cat-nav-head">
                        <div className="row">
                            <MainAllCategory />
                            <MenuArea />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MainHeaderInner