import React, { Component } from 'react';
import {connect} from "react-redux"
import "./Profile.css"
import HeaderInner from '../Component/HeaderInner';
import {Link} from "react-router-dom"

class Profile extends Component {
    render() {
        let {user} = this.props
        console.log(user.hasOwnProperty("username"))
        return (
            <div>
                <div>
                    <HeaderInner />
                    <div className="col-12 breadcrumbs">
                        <div className="bread-inner">
                            <ul className="bread-list">
                                <li>Trang Chủ<i className="fas fa-arrow-right" /></li>
                                <li className="active">Hồ sơ</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="profile">
                    <h3>HỒ SƠ NGƯỜI DÙNG</h3>
                    {user.hasOwnProperty("username") ? 
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Tên</th>
                            <th scope="col">Tài Khoản</th>
                            <th scope="col">Tuổi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row"><i className="far fa-user icon-user"></i></th>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.age}</td>
                        </tr>
                        </tbody>
                    </table> : 
                    <div className="note">
                        Bạn chưa đăng nhập tài khoản
                        <div className="login"><Link to="/login">Đăng nhập</Link></div>
                    </div>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state=>({
    user : state.user
})

export default connect(mapStateToProps, null)(Profile);