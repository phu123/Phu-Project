import React,{Component} from "react"
import MainHeaderInner from "./../Component/ViewHome/HeaderInner/MainHeaderInner"
import MainSliderArea from "./../Component/ViewHome/SliderArea/MainSliderArea"
import MainSmallBanner from "./../Component/ViewHome/SmallBanner/MainSmallBanner"
import MainProductInfo from "../Component/ViewHome/ProductInfo/MainProductInfo"
import MainShopBlog from "../Component/ViewHome/ShopBlog/MainShopBlog"
import ShopServices from "./../Component/ViewHome/ShopServices/ShopServices"
import MainDiscountProduct from "../Component/ViewHome/DiscountProduct/MainDiscountProduct"

class Home extends Component{
    render() {
        return (
            <div>
                <MainHeaderInner />
                <MainSliderArea />
                <MainSmallBanner />
                <MainDiscountProduct />
                <MainProductInfo />
                <MainShopBlog />
                <ShopServices />
            </div>
        );
    }
}

export default Home