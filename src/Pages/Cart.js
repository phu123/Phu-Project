import React, { Component } from 'react';
import PropTypes from "prop-types";
import "./Cart.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import NumberFormat from 'react-number-format';
import {addToCart, changeItemQuantity, removeItem} from "./../redux/actions";
import BreadcrumbsCart from "./../Component/ViewCart/BreadcrumbsCart"
import HeaderInner from "./../Component/HeaderInner"

class Cart extends Component {
    handleClickIncrement = (item) => {
      const itemId = item.id;
      const itemSize = item.size
      const itemColor = item.color
      const itemQuantity = item.quantity + 1;
      this.props.changeItemQuantity(itemId, itemQuantity,itemSize, itemColor );
      console.log(itemSize,itemColor)
    };

    handleClickDecrement = (item) => {
      if (item.quantity === 0) return;
      const itemId = item.id;
      const itemSize = item.size
      const itemColor = item.color
      const itemQuantity = item.quantity - 1;
      this.props.changeItemQuantity( itemId, itemQuantity, itemSize, itemColor  );
    };
    handleRemoveClick = (item) => {
      const itemId = item.id;
      const itemSize = item.size
      const itemColor = item.color
      this.props.removeItem( itemId,itemSize ,itemColor  );
      console.log(itemSize,itemColor )
    };
    render() {
        const { cartList, cartTotal, quantityCart } = this.props;
        console.log(quantityCart);
        return (
          <div>
            <HeaderInner />
            <BreadcrumbsCart />
            <div className="row cart">
              <div className="cart-products-inner">
                <h2>GIỎ HÀNG</h2>
                <hr />
                <ul className="cart-products__products">
                  {cartList.map((item) => {
                    return (
                      <li key={item.id} className="cart-products__product">
                        <div className="cart-products__inner">
                          <div className="cart-products__img">
                            <img src={item.pic} alt=""></img>
                          </div>
                          <div className="cart-products__content">
                            <div className="cart-products__desc">                             
                                {" "}
                                {item.name}{" "}
                              <p className="cart-products__actions">
                                <span
                                  onClick={() => this.handleRemoveClick(item)}
                                  className="cart-products__del"
                                >
                                  Xóa
                                </span>
                              </p>
                            </div>
                            <div className="option-choose">
                              <div>
                                <span className="size">Size: <span className="button-size">{item.size}</span></span>
                              </div>
                              <div>
                                <span className="color">Color: <span className="button-color">{item.color}</span></span>
                              </div>
                            </div>
                            <div className="cart-products__details">
                              <div className="cart-products__pricess">
                                <p className="cart-products__real-prices">
                                  <NumberFormat
                                    value={item.price}
                                    displayType="text"
                                    thousandSeparator={true}
                                    renderText={value => <span>{value.replace(",",".")}</span>}
                                  />{" "}
                                  đ{" "}
                                </p>
                              </div>
                              <div className="cart-products__qty">
                                <div className="CartQty__StyledCartQty-sc-1looi6r-0 jijkkP">
                                  <span
                                    onClick={() => this.handleClickDecrement(item)}
                                    className="qty-decrease qty-disable"
                                  >
                                    -
                                  </span>
                                  <input
                                    onChange={(value) =>
                                      this.handleChangeQuantity(item, value)
                                    }
                                    type="number"
                                    name={item.name}
                                    value={item.quantity}
                                    className="qty-input"
                                  ></input>
                                  <span
                                    onClick={() => this.handleClickIncrement(item)}
                                    className="qty-increase "
                                  >
                                    +
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className="prices">
                <p className="prices__total">
                  <span className="prices__text">TỔNG CỘNG</span>
                  <span className="prices__value--final">
                    <NumberFormat
                      value={cartTotal}
                      displayType="text"
                      thousandSeparator={true}
                      renderText={value => <span>{value.replace(",",".")}</span>}
                    />{" "}đ
                  </span>
                </p>
              </div>
            </div>
          </div>
        );
    }
}

Cart.propTypes = {
  changeItemQuantity: PropTypes.func.isRequired,
};

const mapStateToProps = state =>({
    cartList: state.cartList,
    cartTotal: state.cartTotal,
    color: state.color,
    size : state.size,
    count: state.count,
    quantityCart : state.quantityCart
})

const mapDispatchToProps = dispatch=>({
    addToCart:()=>dispatch(addToCart()),
    changeItemQuantity:(a,b,c,d)=>dispatch(changeItemQuantity(a,b,c,d)),
    removeItem:(e,f,g)=>dispatch(removeItem(e,f,g))
})


export default connect(mapStateToProps, mapDispatchToProps)(Cart);