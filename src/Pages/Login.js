import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import HeaderInner from "../Component/HeaderInner"
import "./Login.css";
import { login, logout } from "../redux/actions";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  handleLogin = async (e) => {
    e.preventDefault();
    console.log(this.state.username, this.state.password);
    await this.props.login({
      ...this.state,
    });

    this.props.history.push("/");
  };

  handleAlert=()=>{
    alert("Bạn chưa đăng nhập tài khoản")
  }

  render() {
    return (
      <div>
        <div>
          <HeaderInner />
          <div className="col-12 breadcrumbs">
            <div className="bread-inner">
              <ul className="bread-list">
                <li>Trang Chủ<i className="fas fa-arrow-right" /></li>
                <li className="active">Đăng nhập</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="login">
          <form action="index.html" className="formdata">
            <div className="formdata__title">ĐĂNG NHẬP</div>
            <div className="formdata__inf">
              <label htmlFor="email">Tên đăng nhập:</label>
              <br />
              <input
                type="text"
                id="email"
                className="formdata__boxinf"
                placeholder="User..."
                required
                onChange={(e) =>
                  this.setState({
                    ...this.state,
                    username: e.target.value,
                  })
                }
              />
            </div>
            <div className="formdata__inf">
              <label htmlFor="password">Mật Khẩu:</label>
              <br />
              <input
                type="password"
                id="password"
                className="formdata__boxinf"
                placeholder="Password..."
                required
                onChange={(e) =>
                  this.setState({
                    ...this.state,
                    password: e.target.value,
                  })
                }
              />
            </div>
            <div className="check">
              <div>
                <input type="checkbox" id="Checkid" />
                <label htmlFor="Checkid">Lưu mật khẩu</label>
              </div>
              <span className="check__forgot">Quên mật khẩu?</span>
            </div>
            <button
              type="submit"
              onClick={this.handleLogin}
              className="formdata__boxinf signin"
            >
              Đăng Nhập
            </button>
            <div className="or">
              <span className="or__text">or</span>
              <div className="line" />
            </div>
            <button type="button" className="formdata__boxinf registerbutton" onClick={this.props.logout}>                       
              Đăng Xuất
            </button> 
          </form>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  login: (data) => dispatch(login(data)),
  logout : ()=>dispatch(logout())
});

export default connect(null, mapDispatchToProps)(withRouter(Login));
