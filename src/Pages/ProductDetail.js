import React, { Component } from 'react';
import HeaderInner from "./../Component/HeaderInner"
import BreadcrumbsDetail from "../Component/ViewProductDetail/BreadcrumbsDetail";
import MainProductDetail from '../Component/ViewProductDetail/MainProductDetail';
import "./ProductDetail.css"
import InfoProductDetail from "./../Component/ViewProductDetail/InfoProductDetail"
import MainInfoShop from "./../Component/ViewProductDetail/InfoShop/MainInfoShop"

class ProductDetail extends Component {
    render() {
        return (
            <div>
                <HeaderInner />
                <BreadcrumbsDetail />
                <div className="product-detail">
                    <MainProductDetail />
                    <InfoProductDetail />
                </div>
                <div>
                    <MainInfoShop/>
                </div>
            </div>
        );
    }
}

export default ProductDetail;