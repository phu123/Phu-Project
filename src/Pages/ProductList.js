import React, {Component} from "react"
import "./ProductList.css"
import MainFilter from "../Component/ViewProductList/Filter/MainFilter";
import MainProduct from "../Component/ViewProductList/Product/MainProduct";
import Breadcrumbs from "../Component/ViewProductList/Breadcrumbs/Breadcrumbs";
import HeaderInner from "../Component/HeaderInner";
import Test from "../Test";

class ProductList extends Component{
    render() {
        return (
            <div>
                <HeaderInner />
                <Breadcrumbs />
                <div className="row productlist">
                    <MainFilter />
                    <MainProduct />
                </div>
            </div>
        );
    }
}

export default ProductList