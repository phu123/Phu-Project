import {
  GET_DATA,
  FILTER_CHECKBOX,
  DATA_PREVIEW,
  FILTER_PRICE,
  DATA_PRODUCT_DETAIL,
  CHANGE_IMG_DETAIL,
  INCREASE_COUNT,
  DECREASE_COUNT,
  SIZE,
  COLOR,
  ADD_TO_CART,
  WARNING,
  FIX_WARNING,
  CHANGE_ITEM_QUANTITY,
  REMOVE_ITEM,
  FILTERLOCATION,
  LOGIN,
  SEARCH,
  LOGOUT,
  SET_ORDER_NAMEAZ,
  SET_ORDER_NAMEZA,
  SET_ORDER_PRICEAZ,
  SET_ORDER_PRICEZA 
} from "./actions";

const initialState = {
  productList: [],
  obj: {},
  user: {
    username: "",
    password: "",
  },
  productPreview: [],
  query: "ao khoac",
  dataUser: "",
  filterPrice: [],
  filterCheckbox: [],
  filterLocation: [],
  dataPrice: [],
  dataLocal: [],
  productListFinal: [],
  product: {},
  images: "",
  count: 1,
  size: "",
  color: "",
  cartList: [],
  cartTotal: 0,
  warning: false,
  quantityCart : 0,
  user : {},
  ordervalue1 : "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA: {
      return {
        ...state,
        productList: [...action.productList],
        productListFinal: [...action.productList],
      };
    }
    case DATA_PREVIEW: {
      return { ...state, productPreview: [...action.productPreview] };
    }
    case FILTER_CHECKBOX: {
      let newObj = state.obj;
      if (
        Object.values(newObj).indexOf(action.value) !== -1 &&
        Object.keys(newObj).indexOf(action.key) !== -1
      ) {
        delete newObj[action.key];
      } else {
        newObj[action.key] = action.value;
      }
      const result = [...state.productList].filter((product) => {
        let a = true;
        for (let key in newObj) {
          if (newObj[key] === product[key]) {
            a = true;
          } else {
            a = false;
            break;
          }
        }
        return a;
      });
      const concatArr = result.concat(state.filterPrice, state.filterLocation);
      const lookup = concatArr.reduce((a, e) => {
        a[e.id] = ++a[e.id] || 0;
        return a;
      }, {});
      const arr = concatArr.filter((e) => lookup[e.id]);
      let arrFinal;
      if (arr.length !== 0) {
        const filterArrFinal = arr.reduce((acc, current) => {
          const x = acc.find((item) => item.id === current.id);
          if (!x) {
            return acc.concat([current]);
          } else {
            return acc;
          }
        }, []);
        arrFinal = filterArrFinal;
      } else {
        arrFinal = concatArr;
      }
      return { ...state, productListFinal: arrFinal, filterCheckbox: result};
    }
    case SEARCH: { 
      return { ...state, query: action.dataUser};
    }
    case FILTERLOCATION: {
      let datalLocation = state.dataLocal;
      if (datalLocation.length === 0) {
        datalLocation.push(action.local);
      } else {
        if (!datalLocation.includes(action.local)) {
          datalLocation.push(action.local);
        } else {
          const index = datalLocation.findIndex((x) => x === action.local);
          datalLocation.splice(index, 1);
        }
      }
      let result;
      if (datalLocation.length !== 0) {
        result = [...state.productList].filter(
          (arr) =>
            arr.shop_warehouse_city === datalLocation[0] ||
            arr.shop_warehouse_city === datalLocation[1] ||
            arr.shop_warehouse_city === datalLocation[2]
        );
      } else {
        result = [...state.productList];
      }
      console.log(result);
      const concatArr = result.concat(state.filterPrice, state.filterCheckbox);
      const lookup = concatArr.reduce((a, e) => {
        a[e.id] = ++a[e.id] || 0;
        return a;
      }, {});
      const arr = concatArr.filter((e) => lookup[e.id]);
      let arrFinal;
      if (arr.length !== 0) {
        const filterArrFinal = arr.reduce((acc, current) => {
          const x = acc.find((item) => item.id === current.id);
          if (!x) {
            return acc.concat([current]);
          } else {
            return acc;
          }
        }, []);
        arrFinal = filterArrFinal;
      } else {
        arrFinal = concatArr;
      }
      return { ...state, productListFinal: arrFinal, filterLocation: result };
    }
    case FILTER_PRICE: {
      let price = [...state.dataPrice];
      if (price.length === 0) {
        price.push(action.min, action.max);
      } else {
        if (price[0] === action.min && price[1] === action.max) {
          price.splice(0, 2);
        } else {
          price.splice(0, 2, action.min, action.max);
        }
      }
      console.log(price)
      let result = [...state.productList].filter((product) => {
        if (action.max !== -1 && price.length !== 0) {
          return (
            product.final_price >= price[0] && product.final_price <= price[1]
          );
        } else if (action.max === -1 && price.length !== 0){
          return product.final_price > price[0];
        } else if (price.length === 0) {
          return [...state.productList];
        }
      });
      const concatArr = result.concat(
        state.filterCheckbox,
        state.filterLocation
      );
      const lookup = concatArr.reduce((a, e) => {
        a[e.id] = ++a[e.id] || 0;
        return a;
      }, {});
      const arr = concatArr.filter((e) => lookup[e.id]);
      let arrFinal;
      if (arr.length !== 0) {
        const filterArrFinal = arr.reduce((acc, current) => {
          const x = acc.find((item) => item.id === current.id);
          if (!x) {
            return acc.concat([current]);
          } else {
            return acc;
          }
        }, []);
        arrFinal = filterArrFinal;
      } else {
        arrFinal = concatArr;
      }

      return {
        ...state,
        productListFinal: arrFinal,
        dataPrice: price,
        filterPrice: result,
      };
    }
    case DATA_PRODUCT_DETAIL: {
      return {
        ...state,
        product: { ...action.product },
        images:
          action.product && action.product.images && action.product.images[0],
      };
    }
    case CHANGE_IMG_DETAIL: {
      return { ...state, images: action.img };
    }
    case INCREASE_COUNT: {
      return { ...state, count: state.count + 1 };
    }
    case DECREASE_COUNT: {
      return { ...state, count: state.count - 1 };
    }
    case SIZE: {
      return { ...state, size: action.sizeProduct };
    }
    case COLOR: {
      return { ...state, color: action.colorProduct };
    }
    case ADD_TO_CART: {
      const item = { ...action.payload };
      const newList = [...state.cartList];
      if (
        !newList.find(
          (x) =>
            x.id === item.id && x.size === state.size && x.color === state.color
        )
      ) {
        newList.push(item);
      } else if (
        newList.find(
          (x) =>
            x.id === item.id && x.size === state.size && x.color === state.color
        )
      ) {
        const ItemIndex = newList.findIndex((x) => x.id === item.id);
        console.log(ItemIndex);
        newList[ItemIndex] = {
          ...newList[ItemIndex],
          quantity: newList[ItemIndex].quantity + 1,
        };
      } else if (
        newList.find(
          (x) =>
            x.id === item.id &&
            (x.size !== state.size || x.color !== state.color)
        )
      ) {
        newList.push(item);
      }
      const newCartTotal = newList.reduce(
        (a, b) => a + b.quantity * b.price,
        0
      );
      const quantity = newList.reduce(
        (c, d) => c + d.quantity, 0
      )
      return {
        ...state,
        cartList: newList,
        cartTotal: newCartTotal,
        quantityCart : quantity
      };
    }
    case WARNING: {
      return { ...state, warning: true };
    }
    case FIX_WARNING: {
      return { ...state, warning: false, size : "", color:"" };
    }
    case CHANGE_ITEM_QUANTITY: {
      const itemId = action.itemId;
      const itemQuantity = action.itemQuantity;
      const itemColor = action.itemColor;
      const itemSize = action.itemSize;
      const newCart = [...state.cartList];
      const ItemIndex = newCart.findIndex(
        (item) =>
          item.id === itemId &&
          item.size === itemSize &&
          item.color === itemColor
      );
      newCart[ItemIndex] = {
        ...newCart[ItemIndex],
        quantity: itemQuantity,
      };

      const newCartTotal = newCart.reduce(
        (a, b) => a + b.quantity * b.price,
        0
      );
      const quantity = newCart.reduce(
        (c, d) => c + d.quantity, 0
      )

      return {
        ...state,
        cartList: newCart,
        cartTotal: newCartTotal,
        quantityCart : quantity
      };
    }
    case REMOVE_ITEM: {
      const itemId = action.itemId;
      const itemColor = action.itemColor;
      const itemSize = action.itemSize;
      const newList = [...state.cartList].filter(
        (item) =>
          !(
            item.size === itemSize &&
            item.id === itemId &&
            item.color === itemColor
          )
      );

      const newCartTotal = newList.reduce(
        (a, b) => a + b.quantity * b.price,
        0
      );
      const quantity = newList.reduce(
        (c, d) => c + d.quantity, 0
      )
      return {
        ...state,
        cartList: newList,
        cartTotal: newCartTotal,
        quantityCart : quantity
      };
    }
    case LOGIN: {
      return {
        ...state,
        user: {...action.user}
      };
    }
    case LOGOUT: {
      return {
        ...state,
        user: {}
      };
    }
    case SET_ORDER_NAMEAZ:{
      var arr = [...state.productListFinal].sort((a, b)=>{
        var nA = a.name.toLowerCase()
        var nB = b.name.toLowerCase()

        if(nA < nB)
          return -1;
        else if(nA > nB)
          return 1;
        return 0;
      });
      return {...state, productListFinal: arr}
    }
    case SET_ORDER_NAMEZA:{
      var arr = [...state.productListFinal].sort((a, b)=>{
        var nA = a.name.toLowerCase()
        var nB = b.name.toLowerCase()

        if(nA > nB)
          return -1;
        else if(nA < nB)
          return 1;
        return 0;
      });
      return {...state, productListFinal: arr}
    }
    case SET_ORDER_PRICEAZ:{
      var arr = [...state.productListFinal].sort((a, b)=>{
        var nA = a.final_price
        var nB = b.final_price

        if(nA < nB)
          return -1;
        else if(nA > nB)
          return 1;
        return 0;
      });
      return {...state, productListFinal: arr}
    }
    case SET_ORDER_PRICEZA:{
      var arr = [...state.productListFinal].sort((a, b)=>{
        var nA = a.final_price
        var nB = b.final_price

        if(nA > nB)
          return -1;
        else if(nA < nB)
          return 1;
        return 0;
      });
      return {...state, productListFinal: arr}
    }
    default:
      return state;
  }
};

export default reducer;
